/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const graph = require('@microsoft/microsoft-graph-client');
require('isomorphic-fetch');

var exports = module.exports = {}

exports.getUserDetails = async function(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const user = await client.api('/me').get();

  return user;
}

exports.getMessageCount = async function(accessToken, folder) {
  const client = getAuthenticatedClient(accessToken);

  return await client
    .api('/me/mailFolders/' + folder)
    .count(true)
    .get();
}

exports.getMessages = async function(accessToken, folder, offset, itemPerPage) {
  const client = getAuthenticatedClient(accessToken);

  return await client
    .api('/me/mailFolders/' + folder + '/messages')
    .select('id,sender,toRecipients,ccRecipients,subject,isRead,receivedDateTime,bodyPreview,body,webLink')
    .orderby('receivedDateTime DESC')
    .count(true)
    .skip(offset)
    .top(itemPerPage)
    .get();
}

function getAuthenticatedClient(accessToken) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: (done) => {
      done(null, accessToken);
    }
  });

  return client;
}