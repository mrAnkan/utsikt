/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const flash = require('connect-flash');
require('dotenv').config();

const db = require('./db.js');
const passport = require('./passport.js');

const authRouter = require('./routes/auth');
const emailsRouter = require('./routes/emails');
const emailRouter = require('./routes/email');

const app = express();

// Configure the database.
(async () => {
   await db.initializeDb();
})();

// <SessionSnippet>
// Session middleware
// NOTE: Uses default in-memory session store, which is not
// suitable for production
app.use(session({
  secret: 'your_secret_value_here',
  resave: false,
  saveUninitialized: false,
  unset: 'destroy'
}));

// Flash middleware
app.use(flash());

// Set up local vars for template layout
app.use(function(req, res, next) {
  // Read any flashed errors and save
  // in the response locals
  res.locals.error = req.flash('error_msg');

  // Check for simple error string and
  // convert to layout's expected format
  const errs = req.flash('error');
  for (let i in errs){
    res.locals.error.push({message: 'An error occurred', debug: errs[i]});
  }

  next();
});
// </SessionSnippet>

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// <FormatDateSnippet>
const hbs = require('hbs');
const moment = require('moment');
// Helper to format date/time sent by Graph
hbs.registerHelper('swedishDateTime', function(dateTime){
  return moment(dateTime).format('YYYY-MM-DD HH:MM');
});
// </FormatDateSnippet>

hbs.registerHelper('times', function(n, block) {
  let accumulator = '';
  
  for (var i = 1; i <= n; ++i)
    accumulator += block.fn(i);

  return accumulator;
});

hbs.registerHelper('addition', function(value, options) {
  let current = parseInt(value);
  return ++current;
});

hbs.registerHelper('subtraction', function(value, options) {
  let current = parseInt(value);
  return --current;
});

hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
  return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Initialize passport
app.use(passport.initialize());
app.use(passport.session());

// <AddProfileSnippet>
app.use(function(req, res, next) {
  // Set the authenticated user in the
  // template locals
  if (req.user) {
    res.locals.user = req.user.profile;
  }
  next();
});
// </AddProfileSnippet>

app.use('/auth', authRouter);
app.use('/', emailsRouter);
app.use('/api/email', emailRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
