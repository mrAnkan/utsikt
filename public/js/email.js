/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

document.querySelectorAll('.open-issue')
  .forEach(element => element.addEventListener("click", async function(event) {
    event.preventDefault();

    const prev = event.target.innerHTML;
    event.target.innerHTML = 
    `
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    `;

    const res = await axios.post(`/api/email/${this.dataset.messageid}/issue`, { 
      summary: this.dataset.summary, 
      description: this.dataset.description
    });

    event.target.innerHTML = prev;

    document.getElementById(`issue-links-${this.dataset.messageid}`).insertAdjacentHTML('beforeend',`
      <li class="horizontal-link">
        <a href="" target="_blank">${res.data.key}</a>
      </li>
    `);
  }));