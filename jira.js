/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const JiraApi = require('jira-client');

const jira = new JiraApi({
  protocol: 'https',
  host: process.env.JIRA_HOST,
  username: process.env.JIRA_USER,
  password: process.env.JIRA_PASS,
  apiVersion: '2',
  strictSSL: true
});

var exports = module.exports = {}

exports.getIssue = async function() {
  return {}
}

exports.createIssue = async function(summary, description) {
  const issue = {
    "fields": {
      "summary": summary,
      "issuetype": {
        "id": "10001"
      },
      "project": {
        "id": "10000"
      },
      "description": description,
      "reporter": {
        "id": "557058:1d214abf-24dd-4a14-9904-2094bdef77a6"
      },
      "labels": [
        "bugfix"
      ],
      "assignee": {
        "id": "557058:1d214abf-24dd-4a14-9904-2094bdef77a6"
      }
    }
  }

  return await jira.addNewIssue(issue);
}