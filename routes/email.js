/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const express = require('express');
const router = express.Router();
const db = require('../db.js');
const jira = require('../jira.js')

router.get('/:emailId', async function(req, res) {
  if (!req.isAuthenticated()) {
    res.render('index');
  } else {
    res.redirect('/');
  }
});

router.get('/:emailId/issue', async function(req, res) {
  if (!req.isAuthenticated()) {
    res.render('index');
  } else {
    const id = await db.getIssueKeys(req.params.emailId);
    
    if(id) {      
      const issue = await jira.getIssue(id);

      res.json(issue);
    } else {
      res.redirect('/');
    }
  }
});

router.post('/:emailId/issue', async function(req, res) {
  if (!req.isAuthenticated()) {
    res.render('index');
  } else {
    const issue = await jira.createIssue(req.body.summary, req.body.description);

    db.storeIssueId(req.params.emailId, issue.key, issue.id);

    res.json(issue);
  }
});

module.exports = router;