/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const express = require('express');
const router = express.Router();
const tokens = require('../tokens.js');
const graph = require('../graph.js');
const db = require('../db.js');

const folder = 'inbox';
const itemsPerPage = process.env.MESSAGES_PER_PAGE ? process.env.MESSAGES_PER_PAGE : 10;

router.get('/', async function(req, res) {
  if (!req.isAuthenticated()) {
    res.render('index');
  } else {
    let params = {
      active: { emails: true }
    };

    // Get the access token
    let accessToken;
    try {
      accessToken = await tokens.getAccessToken(req);
    } catch (err) {
      req.flash('error_msg', {
        message: 'Could not get access token. Try signing out and signing in again.',
        debug: JSON.stringify(err)
      });
    }

    if (accessToken && accessToken.length > 0) {
      try {
        const page = req.query.page && req.query.page > 1 ? req.query.page : 1; 
        const offset = (page - 1) * itemsPerPage;

        const messageCount = await graph.getMessageCount(accessToken, folder);
        const messages = await graph.getMessages(accessToken, folder, offset, itemsPerPage);

        for (const message of messages.value) {
          message['issueKeys'] = await db.getIssueKeys(message.id);
        }

        params.unreadCount = messageCount.unreadItemCount;
        params.totalCount = messageCount.totalItemCount;
        params.pageCount = Math.ceil(messageCount.totalItemCount/itemsPerPage);
        params.currentPage = page;
        params.isFirstPage = page == 1;
        params.isLastPage = page == params.pageCount;
        params.hidePagination = messageCount.totalItemCount <= itemsPerPage;
        params.messages = messages.value;
        params.jiraBaseUri = process.env.JIRA_ISSUE_BASE_URI;

      } catch (error) {
        req.flash('error_msg', {
          message: 'Could not fetch emails',
          debug: JSON.stringify(error)
        });
      }
    } else {
      req.flash('error_msg', 'Could not get an access token');
    }

    res.render('emails', params);
  }
});

module.exports = router;