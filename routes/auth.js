/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

var express = require('express');
var passport = require('passport');
var router = express.Router();

router.get('/signin', function(req, res, next) {
  passport.authenticate('azuread-openidconnect',
    {
      response: res,
      prompt: 'login',
      failureRedirect: '/',
      failureFlash: true,
      successRedirect: '/'
    }
  )(req,res,next);
});

router.post('/callback', function(req, res, next) {
  passport.authenticate('azuread-openidconnect',
    {
      response: res,
      failureRedirect: '/',
      failureFlash: true,
      successRedirect: '/'
    }
  )(req,res,next);
});

router.get('/signout', function(req, res) {
  req.session.destroy(function(err) {
    req.logout();
    res.redirect('/');
  });
});

module.exports = router;
