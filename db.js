/**
 * Utsikt - An email program that connects Microsoft Outlook and Atlassian Jira.
 * 
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  
 * This program is based on scaffolding by Microsoft Corporation.  
 * Copyright (c) Microsoft Corporation
 * Licensed under the MIT License.
 */

const sqlite3 = require('sqlite3');
const sqlite = require('sqlite');

var exports = module.exports = {}

let _db;

exports.initializeDb = async function() {
  if(!_db) {
    _db = await sqlite.open({
      'filename': './database.sqlite',
      'driver': sqlite3.Database
    });
  }
}

exports.getIssueIdsByEmailIds = async function(ids) {
  return await _db.all('SELECT * FROM email_issue_mapping WHERE emailId in ( ' 
    + ids.map(function(){ return '?' }).join(',') + ' )', ids);
}

exports.storeIssueId = async function(emailId, issueKey, issueId) {
  return await _db.run('INSERT INTO email_issue_mapping VALUES (?, ?, ?)', [emailId, issueKey, issueId]);
}

exports.getIssueKeys = async function(emailId) {
  const data = await _db.all('SELECT issueKey FROM email_issue_mapping WHERE emailId = ?', emailId)
  return data.map(current => current.issueKey);
}
